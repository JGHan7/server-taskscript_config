# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=


# User specific aliases and functions
source /public1/soft/modules/module.sh
#module load gcc/10.2.0
#module load elpa/2021.05.002
#module load mpi/intel/20.0.4
#module load cmake/3.23.1
#source /public1/soft/oneAPI/2022.1/setvars.sh intel64
#if [ $SSH_TTY  ] ; then
#source /public1/soft/oneAPI/2022.1/setvars.sh intel64
#fi


## by JG

# cmake
export PATH=/public1/home/t6s000394/jghan/software/cmake-3.27.8-linux-x86_64/bin:$PATH

# Internet
export http_proxy=http://10.255.128.100:3128
export https_proxy=http://10.255.128.100:3128
export ftp_proxy=http://10.255.128.100:3128

# abacus by JG
module add gcc/10.2.0
#module add mpi/intel/20.0.4
module add mpi/intel/2021.2 
export LD_LIBRARY_PATH=/public1/soft/gcc/10.2.0/lib64/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/public1/soft/openblas/0.3.17/lib:$LD_LIBRARY_PATH
export PATH=/public1/home/t6s000394/jghan/software/abacus-develop/rdmft-abacus/bin:$PATH

# mkl
# ${MKLROOT}=/public1/soft/intel/2020/compilers_and_libraries_2020.4.304/linux/mkl

## end by JG






# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/public1/home/t6s000394/app/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/public1/home/t6s000394/app/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/public1/home/t6s000394/app/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/public1/home/t6s000394/app/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

